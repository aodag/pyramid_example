from pyramid.config import Configurator
from chamelen_render.resources import Root

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(root_factory=Root, settings=settings)
    config.add_view('chamelen_render.views.my_view',
                    context='chamelen_render:resources.Root',
                    renderer='chamelen_render:templates/index.pt')
    config.add_static_view('static', 'chamelen_render:static', cache_max_age=3600)
    config.add_subscriber('chamelen_render.subscribers.add_base_template',
        'pyramid.events.BeforeRender')
    return config.make_wsgi_app()
